"use strict";
// basic types
let id = 5;
let company = "Traversy Media";
let isPublished = true;
let x = "hello";
//arrays
let ids = [1, 2, 3, 4, 5];
let arr = [1, true, "hello"];
// tupple --->we can specify the exact type in the array element by sequence
let person = [1, "hello", true];
// tupple array
let employee;
employee = [
    [1, "hello"],
    [2, "yudh"],
    [3, "hell"],
];
// unions ---> we have to set the value to string or number when we dont knw what will be there
let productId = 22;
// enum
var direction1;
(function (direction1) {
    direction1[direction1["Up"] = 0] = "Up";
    direction1[direction1["Down"] = 1] = "Down";
    direction1[direction1["left"] = 2] = "left";
    direction1[direction1["Right"] = 3] = "Right";
})(direction1 || (direction1 = {}));
console.log(direction1);
const user = {
    id: 1,
    name: "tushar"
};
//type assertion
let cid = 1;
// let customerId = <number>cid;
// OR
let customerId = cid;
// functions
function addNum(x, y) {
    return x + y;
}
console.log(addNum(1, 3));
//we can send parameters as string or number
function log(message) {
    console.log(message);
}
{
    id: number;
    //ie we cannot chnage the value we can just view it
    name: string;
    age ?  : number; //optional
}
;
const user = {
    id: 1,
    name: "tushar",
    age: 22
};
user.id = 2;
const add = (x, y) => x + y;
const sub = (x, y) => x - y;
// -------------------------------------classes ----------------------------------
class Person {
    constructor(id, name) {
        console.log("constructor runned");
        this.id = id;
        this.name = name;
    }
    register() {
        return `${this.name} is now registered`;
    }
}
const brad = new Person(1, "tushar");
const mike = new Person(2, "rushi");
console.log(brad);
console.log(mike);
console.log(typeof brad);
console.log(mike.register());
;
class Person {
    constructor(id, name) {
        console.log("constructor runned");
        this.id = id;
        this.name = name;
    }
    register() {
        return `${this.name} is now registered`;
    }
}
// to extend the classes
class Employee extends Person {
    constructor(id, name, position) {
        super(id, name); ///this will call the constructor of person class
        this.position = position;
    }
}
const emp = new Employee(3, "Shwan", "developer");
console.log(emp);
