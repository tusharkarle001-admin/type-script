// basic types

let id: number = 5;
let company: string = "Traversy Media";
let isPublished: boolean = true;
let x: any = "hello";

//arrays
let ids: number[] = [1, 2, 3, 4, 5];
let arr: any[] = [1, true, "hello"];

// tupple --->we can specify the exact type in the array element by sequence
let person: [number, string, boolean] = [1, "hello", true];

// tupple array
let employee: [number, string][];
employee = [
	[1, "hello"],
	[2, "yudh"],
	[3, "hell"],
];

// unions ---> we have to set the value to string or number when we dont knw what will be there

let productId: string | number = 22;

// enum
enum direction1 {
	Up,
	Down,
	left,
	Right,
}
console.log(direction1);

// Objects


//to define user defined type
type User = {
    id: number,
    name:string
}

const user:User= {
    id: 1,
    name:"tushar"
}


//type assertion
let cid: any = 1;
// let customerId = <number>cid;
// OR
let customerId = cid as number;


// functions
function addNum(x:number, y:number) :number {
    return x + y;
}
console.log(addNum(1, 3));


//we can send parameters as string or number
function log(message: string | number):void {
    console.log(message);
}


//interfaces
/* it is same as type ie define what value an object will have 
    cons:- we cannot use interfaces with the premitives or unions ie number | string
*/
interface userInterface ={
	readonly id: number;
     //ie we cannot chnage the value we can just view it
    name: string;
    age?:number   //optional
};

const user: userInterface = {
	id: 1,
    name: "tushar",
    age:22
};

user.id = 2;

//--------------------------- interfaces with the functions--------------------------
interface MatchFunc{
    //parameters : return type
    (x:number, y:number):number
}

const add: MatchFunc = (x: number, y: number): number => x + y;
const sub: MatchFunc = (x: number, y: number): number => x - y;


// -------------------------------------classes ----------------------------------

class Person{
//    private id: number,
    id:number,
    name: string,

    constructor(id:number,name:string) {
        console.log("constructor runned");
        this.id = id;
        this.name = name;
    }

    register() {
        return `${this.name} is now registered`;
    }
}

const brad = new Person(1,"tushar");
const mike = new Person(2, "rushi");

console.log(brad);
console.log(mike);
console.log(typeof brad);
console.log(mike.register());

// using interfaces in the classes

interface PersonInterface {
    id: number,
    name: string,
    register():string,
        
};


class Person implements PersonInterface{
    id:number,
    name: string,

    constructor(id:number,name:string) {
        console.log("constructor runned");
        this.id = id;
        this.name = name;
    }

    register() {
        return `${this.name} is now registered`;
    }
}


// to extend the classes

class Employee extends Person {
    position: string,
    
    constructor(id:number,name:string,position:string) {
        super(id, name); ///this will call the constructor of person class
        this.position = position;
    }
}

const emp = new Employee(3, "Shwan", "developer");
console.log(emp);



//generics  ---> it is used to build the reusable components
/* for using generics we use T[] instead of any[] datatype ...it will be replaced by the datatype in <number> after function calling */
function getArray<T>(items: T[]): T[]{
    return new Array().concat(items);
}

let numArray = getArray<number>([1, 2, 3, 4]);
let strArray = getArray<string>(["Brad", "Jhon", "Jill"]);

strArray.push("hello");